package com.easycoding.demo.collection

class CollectionTest {

}

fun printAll(strings: Collection<String>) {
    for(s in strings) print("$s ")
    println()
}

fun main(args: Array<String>) {
/**
 * kotlin: https://www.kotlincn.net/docs/reference/collection-ordering.html
 * */
    val numbers = mutableListOf("one","two","three")
    numbers.add("four")
    println(numbers)
    val stringList = listOf("one", "two", "one")
    printAll(stringList)
    val stringSet = setOf("one", "two", "three")
    printAll(stringSet)
    val number = setOf(1, 2, 3, 4)
    println("Number of elements: ${numbers.size}")
    if (number.contains(1)) println("1 is in the set")
    /**
     * Map
     * */
    val numbersMap = mapOf("key1" to 1, "key2" to 2, "key3" to 3, "key4" to 1)
    println(numbersMap)
    /**
     * filter用法
     * */
    val numberList = listOf("one", "two", "three", "four")
    val longerThan3 = numberList.filter { it.length > 3 }
    println(longerThan3)
    /**
     * 迭代器用法
     * */
    val number1 = listOf("one", "two", "three", "four")
    val numbersIterator = number1.iterator()
    println("********迭代器用法***************")
    while (numbersIterator.hasNext()) {
        println(numbersIterator.next())
    }
    println("********区间与数列***************")
    for (i in 1..4) println(i)
    println("********集合转换***************")
    val numberSet = setOf(1, 2, 3)
    println(numberSet.map { it * 3 })
    println(numberSet.mapIndexed { idx, value -> value * idx })

    println("********分组***************")
    val numberGroup = listOf("one", "two", "three", "four", "five", "six")
    println(numberGroup.groupingBy { it.first() }.eachCount())
    println("********取集合的一部分***************")
    val numberPart = listOf("one", "two", "three", "four", "five", "six")
    println(numberPart.slice(1..3))
    println(numberPart.slice(0..4 step 2))
    println(numberPart.slice(setOf(3, 5, 0)))
    println("********按位置取***************")
    val numberLocation = linkedSetOf("one", "two", "three", "four", "five")
    println(numberLocation.elementAt(3))
    val numbersSortedSet = sortedSetOf("one", "two", "three", "four")
    println(numbersSortedSet.elementAt(0)) //
    println("********排序***************")
    val numberSort = listOf("one", "two", "three", "four")
    println("Sorted ascending: ${numberSort.sorted()}")
    println("Sorted descending: ${numberSort.sortedDescending()}")
    val numberDaoXu = listOf("one", "two", "three", "four")
    println(numberDaoXu.reversed())
    println("********集合写操作***************")
    val numberCollection = mutableListOf(1, 2, 3, 4)
    numberCollection.add(5)
    println(numberCollection)
    numberCollection.remove(1)
    println(numberCollection)
    println("********按索引取元素***************")
    val numberListPart = listOf(1, 2, 3, 4)
    println(numberListPart.get(0))
    println(numberListPart[0])
    //numbers.get(5)                         // exception!
    println(numberListPart.getOrNull(5))             // null
    println(numberListPart.getOrElse(5, {it}))
    println("********取键值对***************")
    val numberMap = mapOf("one" to 1, "two" to 2, "three" to 3)
    println(numberMap.keys)
    println(numberMap.values)
}

