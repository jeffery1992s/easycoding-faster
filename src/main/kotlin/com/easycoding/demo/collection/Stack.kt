package com.easycoding.demo.collection

/**
 * @Description
 * @author chunming.jiang
 * @date 2019/11/20
 **/
/**
 *  栈： 先进后出 或者说 后进先出  数组大小未知 每次插入需要扩充需要把旧数组赋值给新数组 性能消耗 使用贪心算法，数组每次被填满后，加入下一个元素时，把数组拓展成现有数组的两倍大小
 *  队列：先进先出
 *
 *  栈 ADT顺序 栈的应用 栈与递归
 *      ArrayList和LinkList支持栈操作 操作栈顶元素 栈本身是一个线性表 仅限表尾插入和删除 LIFO
 *      递归调用就是栈 栈有后进先出特点 递归调用实质就是循环调用
 *
 * */
class Stack {


}