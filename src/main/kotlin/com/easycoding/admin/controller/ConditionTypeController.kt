package com.easycoding.admin.controller

import com.easycoding.admin.dto.ConditionDto
import com.easycoding.admin.dto.ConditionTypeDto
import com.easycoding.admin.service.ConditionTypeService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * @Description
 * @author
 * @date 2019/10/29
 **/
@Api(tags = arrayOf("基础配置API"), description = "此API主要xxxx.")
@RestController()
@RequestMapping("/condition")
class ConditionTypeController {
    @Autowired
    lateinit var conditionTypeService: ConditionTypeService

    @ApiOperation(value = "新增xxx", notes = "应用场景及说明: xxx")
    @PostMapping
    fun insert(@RequestBody dto: ConditionTypeDto): ConditionDto {
        return conditionTypeService.create(dto)
    }

    @ApiOperation(value = "更新xxx", notes = "应用场景及说明: xxx")
    @PutMapping
    fun update(@RequestBody  dto: ConditionDto): ConditionDto {
        return conditionTypeService.create(dto)
    }

    @ApiOperation(value = "删除xxx", notes = "应用场景及说明: xxx")
    @DeleteMapping
    fun insert(@PathVariable("id") id: Long) {
        conditionTypeService.delete(id)
    }
}