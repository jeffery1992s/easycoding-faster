package com.easycoding.admin.controller

import com.easycoding.admin.dao.ArticleRepository
import com.easycoding.admin.entity.Article
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.*

@RestController
@Api(tags = arrayOf("Easycoding API"),description = "API文档设计版本规范-v.1")
@RequestMapping("/article")
class ArticleController {
    @Autowired
    lateinit var ArticleRepository:ArticleRepository

    @ApiOperation(value="自带分页查询",notes = "应用场景：场景描述")
    @GetMapping(value = "/")
    fun page(@RequestParam(value = "page",defaultValue = "0" ) page:Int,
             @RequestParam(value = "size",defaultValue = "10") size:Int): Page<Article> {
        return ArticleRepository.findAll(PageRequest.of(page,size))
    }

    @ApiOperation(value="根据id查询",notes = "应用场景：场景描述")
    @GetMapping("/{id}")
    fun details(@PathVariable("id") id:Long):Article{
        return ArticleRepository.getOne(id)
    }

    @ApiOperation(value="Testing-POST",notes = "应用场景：测试")
    @PostMapping()
    fun test(){
        return
    }

    @ApiOperation(value = "Testing-DELETE",notes = "应用场景：xxx")
    @DeleteMapping()
    fun testd(){}

    @ApiOperation(value = "Testing-PUT",notes = "应用场景：xxx")
    @PutMapping()
    fun testp(){}
}