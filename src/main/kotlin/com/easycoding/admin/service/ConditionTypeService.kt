package com.easycoding.admin.service

import com.easycoding.admin.dao.ConditionTypeRepository
import com.easycoding.admin.dto.ConditionDto
import com.easycoding.admin.dto.ConditionTypeDto
import com.easycoding.admin.entity.ConditionType
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

/**
 * @Description
 * @author
 * @date 2010/10/31
 **/
@Service
class ConditionTypeService {
    val logger = LoggerFactory.getLogger(ConditionTypeService::class.java)
    @Autowired lateinit var conditionTypeRepository:ConditionTypeRepository

    fun create(dto: ConditionTypeDto): ConditionDto{
        return conditionTypeRepository.save(ConditionType(null,dto.code!!,dto.name, dto.expr!!, dto.modelMappingId).apply {
            this.createAt = Date()
            this.updateAt = Date()
        }).toConditionDto()
    }

    fun update(dto:ConditionDto):ConditionDto{
        return conditionTypeRepository.save(ConditionType(dto.id,dto.code!!,dto.name, dto.expr!!, dto.modelMappingId).apply {
            this.updateAt = Date()
        }).toConditionDto()
    }

    fun delete(id:Long){
        conditionTypeRepository.deleteById(id)
    }

    fun query(){

    }
}