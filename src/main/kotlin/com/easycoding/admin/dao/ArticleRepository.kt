package com.easycoding.admin.dao

import com.easycoding.admin.entity.Article
import org.springframework.data.jpa.repository.JpaRepository

interface ArticleRepository:JpaRepository<Article,Long>{

}