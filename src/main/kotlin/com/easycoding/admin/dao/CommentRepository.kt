package com.easycoding.admin.dao

import com.easycoding.admin.entity.Comment
import org.springframework.data.jpa.repository.JpaRepository

interface CommentRepository :JpaRepository<Comment,Long>{

}