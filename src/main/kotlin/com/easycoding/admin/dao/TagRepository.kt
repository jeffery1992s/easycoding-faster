package com.easycoding.admin.dao

import com.easycoding.admin.entity.Tag
import org.springframework.data.jpa.repository.JpaRepository

interface TagRepository :JpaRepository<Tag,Long>{}