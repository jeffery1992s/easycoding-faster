package com.easycoding.admin.dao

import com.easycoding.admin.entity.ConditionType
import org.springframework.data.jpa.repository.JpaRepository

/**
 * @Description
 * @author
 * @date 2019/10/29
 **/
interface ConditionTypeRepository :JpaRepository<ConditionType,Long>{
}