package com.easycoding.admin.dto

import com.easycoding.admin.enum.ConditionExprEnum
import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

/**
 * @Description
 * @author
 * @date 2019/10/29
 **/
class ConditionDto : ConditionTypeDto() {
    @ApiModelProperty(value = "id ", dataType = "Long", required = false)
    var id: Long? = null
}
open  class ConditionTypeDto {
    @NotEmpty
    @ApiModelProperty(value = "模型字段 ", dataType = "String", required = true)
    var code: String? = null
    @NotEmpty
    @ApiModelProperty(value = "模型字段名称", dataType = "String", required = true)
    var name: String? = null
    @NotNull
    @ApiModelProperty(value = "表达式，IN,notIn,like,startWith,endWith,equal,notEqual,gt,ge,lt,le ", dataType = "String", required = true)
    var expr: ConditionExprEnum? = null
    //    @ApiModelProperty(value = "创建时间:yyyy-MM-dd HH:mm:ss格式", dataType = "String", required = true)
//    var createAt: Date? = null
//    @ApiModelProperty(value = "修改时间:yyyy-MM-dd HH:mm:ss格式 ", dataType = "String", required = true)
//    var updateAt: Date? = null
    @NotNull(message = "模型字段不能为空")
    @ApiModelProperty(value = "模型映射Id", dataType = "String", required = true,notes = "此modelMappingId对应t_model_mapping表的id")
    var modelMappingId: Long? = null
}