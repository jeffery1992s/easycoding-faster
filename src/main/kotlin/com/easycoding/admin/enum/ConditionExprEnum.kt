package com.easycoding.admin.enum

enum class ConditionExprEnum(var expr: String, var func: (value: String) -> Any) {
    IN("\$in", fun(v): List<String> {
        return v.split(",")
    }),
    notIn("\$nin", IN.func),

    like("\$like", fun(v): String {
        return "*$v*"
    }),
    startWith("\$like", fun(v): String {
        return "$v*"
    }),
    endWith("\$like", fun(v): String {
        return "*$v"
    }),
    equal("\$eq", fun(v): String {
        return "$v"
    }),
    notEqual("\$ne", equal.func),
    gt("\$gt", equal.func),
    ge("\$ge", equal.func),
    lt("\$lt", equal.func),
    le("\$le", equal.func),
}