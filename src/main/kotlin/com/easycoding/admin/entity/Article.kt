package com.easycoding.admin.entity

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*
import javax.persistence.*

@Entity
class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id = -1L
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    var gmtCreate = Date()
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    var gmtModify = Date()
    var isDeleted  = 0
    @Column(length = 100)
    var title =""
    var author =""
    @Column(length = 2000)
    var content =""
    @OneToMany(targetEntity = Tag::class,fetch = FetchType.LAZY)
     lateinit var tags:Set<Tag>
    @OneToMany(targetEntity = Comment::class,fetch = FetchType.LAZY)
    lateinit var comments:Set<Comment>
}