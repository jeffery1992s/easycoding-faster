package com.easycoding.admin.entity

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*
import javax.persistence.*

@Entity
class Tag{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id = -1L
    @Column(length = 100)
    var name =""
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    var gmtCreate = Date()
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    var gmtModify = Date()
    var isDeleted  = 0
}