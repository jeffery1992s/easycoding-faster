package com.easycoding.admin.entity

import com.easycoding.admin.dto.ConditionDto
import com.easycoding.admin.enum.ConditionExprEnum
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
/**
 * @Description
 * @author
 * @date 2019/10/29
 **/
@Entity(name = "t_condition_type")
class ConditionType (
        @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id :Long?=null,
        var code:String?=null,
        var name:String?=null,
        var expr: ConditionExprEnum,
        var model_mapping_id :Long? = null,
        var createAt:Date?=null,
        var updateAt:Date?=null
) {
    constructor(conditionDto: ConditionDto) : this(
            id = conditionDto.id,
            code = conditionDto.code,
            name = conditionDto.name,
            expr = conditionDto.expr!!,
            model_mapping_id = conditionDto.modelMappingId
    )
    fun toConditionDto(): ConditionDto {
        return ConditionDto().also {
            it.id = id
            it.code = code
            it.name = name
            it.expr = expr
            it.modelMappingId = model_mapping_id
        }
    }
}