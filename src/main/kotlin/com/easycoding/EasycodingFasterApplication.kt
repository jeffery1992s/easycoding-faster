package com.easycoding

import com.easycoding.admin.dao.ArticleRepository
import com.easycoding.admin.dao.CommentRepository
import com.easycoding.admin.dao.TagRepository
import com.easycoding.admin.entity.Article
import com.easycoding.admin.entity.Comment
import com.easycoding.admin.entity.Tag
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.context.support.beans
import springfox.documentation.swagger2.annotations.EnableSwagger2

@SpringBootApplication
@EnableSwagger2
@EntityScan(basePackages = ["com.easycoding.admin.entity"])
class EasycodingFasterApplication
fun main(args: Array<String>) {
    SpringApplicationBuilder().initializers(
            beans {
                bean {
                    ApplicationRunner {
                        val articleRepository=ref<ArticleRepository>()
                        val tagRepository = ref<TagRepository>()
                        val commentRepository = ref<CommentRepository>()

                        val article = Article()
                        article.title="Kotlin+SpringBOot"
                        article.author = "admin"
                        article.content="第一次熬夜到现在  因为啥呢"

                        val tag1 = Tag()
                        tag1.name = "Kotlin"
                        val tag2 =  Tag()
                        tag2.name = "SpringBoot"
                        article.tags = setOf(tagRepository.save(tag1),tagRepository.save(tag2))

                        val comments:MutableSet<Comment> = mutableSetOf<Comment>()
                        repeat(10){
                            i->
                            val comment =  Comment()
                            comment.author="admin"
                            comment.content = "评论${i},KSB is good"
                            comments.add(commentRepository.save(comment))
                        }
                        article.comments = comments
                        articleRepository.save(article)
                    }
                }
            }
    ).sources(EasycodingFasterApplication::class.java).run(*args)
//    runApplication<EasycodingFasterApplication>(*args)
}
