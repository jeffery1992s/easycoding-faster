package com.easycoding.demo;

import com.easycoding.javaDataStructure.resource.demo.event.PublisherDemo;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

/**
 * @author chunming.jiang
 * @Description //TODO $
 * @date $ 2019/11/25
 **/
public class ThreadDemo {

    @Autowired
    PublisherDemo publisherDemo;

    @Test
    public void oldRunable() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("The old runable now is using!");
            }
        }).start();
    }

//    @Test
//    public void publisherTest() {
//        publisherDemo.publish(1L, "成功了！");
//    }



}
